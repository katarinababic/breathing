import React from 'react';
import {View, StyleSheet, Animated} from 'react-native';
import Svg, {Path, Circle} from 'react-native-svg';

const styles = StyleSheet.create({
  textView: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export type CircularProgressProps = {
  percentage?: number;
  blankColor?: string;
  donutColor?: string;
  fillColor?: string;
  progressWidth?: number;
  size?: number;
  children?: React.ReactNode;
  outerScale?: number | Animated.Value | Animated.AnimatedInterpolation;
  innerScale?: number | Animated.Value | Animated.AnimatedInterpolation;
};

function generateArc(percentage: any, radius: any) {
  if (percentage === 100) percentage = 99.999;
  const a = (percentage * 2 * Math.PI) / 100; // angle (in radian) depends on percentage
  const r = radius; // radius of the circle
  const rx = r,
    ry = r,
    xAxisRotation = 0;
  let largeArcFlag = 1;
  const sweepFlag = 1,
    x = r + r * Math.sin(a),
    y = r - r * Math.cos(a);
  if (percentage <= 50) {
    largeArcFlag = 0;
  } else {
    largeArcFlag = 1;
  }

  return `A${rx} ${ry} ${xAxisRotation} ${largeArcFlag} ${sweepFlag} ${x} ${y}`;
}

export const CircularProgress: React.FC<CircularProgressProps> = ({
  percentage = 40,
  blankColor = '#eaeaea',
  donutColor = '#43cdcf',
  fillColor = 'white',
  progressWidth = 35,
  size = 100,
  children,
  outerScale,
  innerScale,
}) => {
  const half = size / 2;
  const containerStyle = React.useMemo(() => ({width: size, height: size}), [
    size,
  ]);

  React.useEffect(() => {}, [percentage]);

  return outerScale && innerScale ? (
    <Animated.View style={[containerStyle, {transform: [{scale: outerScale}]}]}>
      <Svg width={size} height={size}>
        <Circle cx={half} cy={half} r={half} fill={blankColor} />
        <Path
          d={`M${half} ${half} L${half} 0 ${generateArc(percentage, half)} Z`}
          fill={donutColor}
        />
        {<Circle cx={half} cy={half} r={progressWidth} fill={fillColor} />}
      </Svg>
      {children ? (
        <Animated.View
          style={[styles.textView, {transform: [{scale: innerScale}]}]}>
          {children}
        </Animated.View>
      ) : null}
    </Animated.View>
  ) : (
    <View style={containerStyle}>
      <Svg width={size} height={size}>
        <Circle cx={half} cy={half} r={half} fill={blankColor} />
        <Path
          d={`M${half} ${half} L${half} 0 ${generateArc(percentage, half)} Z`}
          fill={donutColor}
        />
        {<Circle cx={half} cy={half} r={progressWidth} fill={fillColor} />}
      </Svg>
      {children ? <View style={styles.textView}>{children}</View> : null}
    </View>
  );
};
