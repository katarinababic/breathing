import React from 'react';
import {StyleSheet, View, StatusBar, Animated, Dimensions} from 'react-native';

import {CircularProgress} from './components/CircularProgress';

const {width} = Dimensions.get('window');
const size = width - 128;

const App = () => {
  const [progress, setProgress] = React.useState(0);
  let outerScale = new Animated.Value(1);
  let innerScale = new Animated.Value(1);

  const startAnimation = React.useCallback(() => {
    // scale up both the outer circle and the inner circle
    Animated.parallel([
      Animated.timing(outerScale, {
        toValue: 1.2,
        duration: 4000,
        useNativeDriver: true,
      }),
      Animated.parallel([
        Animated.timing(innerScale, {
          toValue: 1.1,
          duration: 4000,
          useNativeDriver: true,
        }),
      ]),
    ]).start(() => {
      // once the initial animation is over, wait for 4 seconds and then scale down both circles
      const timer = setTimeout(() => {
        Animated.parallel([
          Animated.timing(outerScale, {
            toValue: 1,
            duration: 4000,
            useNativeDriver: true,
          }),
          Animated.timing(innerScale, {
            toValue: 1,
            duration: 4000,
            useNativeDriver: true,
          }),
        ]).start();
      }, 4000);
      return () => {
        clearTimeout(timer);
      };
    });
  }, [innerScale, outerScale]);

  React.useEffect(() => {
    // start the initial animation cycle
    startAnimation();
    // repeat the animation cycle every 12 seconds
    const timer = setInterval(() => {
      startAnimation();
    }, 12000);
    return () => {
      clearInterval(timer);
    };
  }, []);

  React.useEffect(() => {
    /**
     * add progress until it reaches 100, then reset it;
     * add a small value and repeat the process in very small intervals,
     * so that the progress fills up smoothly
     */
    const timer = setInterval(() => {
      if (progress + 0.3 < 100) {
        setProgress(progress + 0.3);
      } else {
        setProgress(100);
        setProgress(0);
      }
    }, 26);

    return () => {
      clearInterval(timer);
    };
  }, [progress]);

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <View style={styles.container}>
        <CircularProgress
          blankColor="#ffcccc"
          donutColor="#f01b42"
          percentage={progress}
          progressWidth={135}
          size={size}
          outerScale={outerScale}
          innerScale={innerScale}>
          <View style={styles.innerCircle} />
        </CircularProgress>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  innerCircle: {
    alignSelf: 'center',
    backgroundColor: '#ffcccc',
    width: size - 56,
    height: size - 56,
    borderRadius: (size - 56) / 2,
  },
});

export default App;
